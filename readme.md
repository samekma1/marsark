## Marsark Personal Gentoo portage Overlay

This repository contains my modified and new ebuilds.

### Usage

More recent versions of Portage allows you to add additional repositories by
adding a config file in `/etc/portage/repos.conf/marsark.conf`. Here's an example:

```ini
[marsark]
location = /var/db/repos/marsark
sync-type = git
sync-uri = https://gitlab.fel.cvut.cz/samekma1/marsark.git
auto-sync = yes
```

Keyword as appropriate and use emerge like you normally would do.
